const handleKeyPress = (e, k, cb) =>
  (e.key === k) &&
  cb();

module.exports = handleKeyPress;