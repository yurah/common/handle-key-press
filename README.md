# @timeyurah/handle-key-press

[![npm (scoped)](https://img.shields.io/badge/npm-v1.0.2-blue.svg)](https://gitlab.com/yurah/shared/handle-key-press)

## Objetivo

Checar qual tecla foi digitada pelo usuário. Caso a tecla seja a escolhida, executar uma função.


## Instalação

```
npm install @timeyurah/handle-key-press
```

## Uso

Em um ambiente React:

```
import React, { Component } from 'react;
import handleKeyPress from '@timeyurah/handle-key-press';

class Test extends Component {
  testFunction = () => {
    console.log('It worked!')
  }

  render = () =>
    <form>
      <input type='text' onKeyPress={ e => handleKeyPress(e, 'Enter', this.testFunction) } />
    </form>
}

export default Test;
```
